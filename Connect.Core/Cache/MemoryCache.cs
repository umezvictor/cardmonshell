﻿using LazyCache;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Connect.Core.Cache
{
    public class MemoryCache : ISharedCache
    {
        private readonly IAppCache _cache;

        public MemoryCache(IAppCache cache)
        {
            _cache = cache;
        }
        public T Get<T>(string key)
        {
            var item = _cache.GetOrAdd<T>(key, factory => default(T));
            return item;
        }

        public void Set<T>(string key, T value)
        {
            _cache.Add<T>(key, value);
        }
    }
}
