﻿using Microsoft.Extensions.Caching.Distributed;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Connect.Core.Cache
{
    public interface ISharedCache
    {
        T Get<T>(string key);
        void Set<T>(string key, T value);
    }
}
