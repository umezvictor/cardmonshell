﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Connect.Core.ViewModels
{
    public class MenuVM
    {
        //public Guid Id { get; set; }
        public string Title { get; set; }
        public string DefaultParameter { get; set; }
        public string DefaultInputType { get; set; }
        public List<string> ExtraParams { get; set; }
        public List<string> ExtraInputTypes { get; set; }


        //title = menuTitle,
        //defaultParameter = menuParameter,
        //defaultInputType = menuInputType,
        //extraParams = extraParamsArray,
        //extraInputTypes = extraInputTypeArray
    }

    public class PageVM
    {
        public string Title { get; set; }
    }

}
