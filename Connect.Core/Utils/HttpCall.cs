﻿using Connect.Core.Utils;
using Connect.Core.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace Connect
{
    public class HttpCall
    {
        public static string PostDataAsync(string data)
        {
            HttpClient client = new HttpClient();
            string uri = "https://api.flutterwave.com/v3/payments";
            string res = "";
            try
            {
               // var config = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("AppConfig");
               // string url = config["url"];


                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", "Bearer FLWSECK_TEST-dc155eb5b7eea8c18e9ae94604bfbfc4-X");
                HttpResponseMessage response = client.PostAsync(uri, new StringContent(data, Encoding.UTF8, "application/json")).Result;

                if (response.IsSuccessStatusCode)
                {
                    var responseData = response.Content.ReadAsStringAsync().Result;                    
                    res = responseData;                  
                }
                else
                {
                    res = response.Content.ReadAsStringAsync().Result;
                }
            }
            catch (Exception ex)
            {
                Logger.writeErrorLog(ex.ToString());
                res = "failed";
            }

            return res;
        }

        //send a get request to a restful web api
        public static string DoHttpGet(string urll)
        {
            string resp = "";
            string url = urll;
            try
            {
                HttpWebRequest req = WebRequest.Create(new Uri(url)) as HttpWebRequest;
                using (HttpWebResponse res = req.GetResponse() as HttpWebResponse)
                {
                    Stream strm = res.GetResponseStream();
                    StreamReader red = new StreamReader(strm);
                    resp = red.ReadLine();
                }
                return resp;
            }
            catch (Exception ex)
            {
                return "ERROR:: " + ex.ToString();
            }
        }

        public static async Task<string> VerifyPayment(string url)
        {
            string res = "";
            try
            {
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Add("Authorization", "Bearer FLWSECK_TEST-dc155eb5b7eea8c18e9ae94604bfbfc4-X");
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.GetAsync(url);
                if (response.IsSuccessStatusCode)
                {
                    res = response.Content.ReadAsStringAsync().Result;
                }
                else
                {
                    res = response.Content.ReadAsStringAsync().Result;
                }

                return res;
            }
            catch(Exception ex)
            {
                Logger.writeErrorLog("ERROR VERIFYING PAYMENT " + ex.Message);
                res = "failed";
                return res;
            }
        }

    }
}

