﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Connect.Core.Extensions
{
    public static class RSAKeyExtensions
    {
        public static void FromXmlString2(this RSA rsa, string xmlString)
        {
            RSAParameters parameters = default(RSAParameters);
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(xmlString);
            if (xmlDocument.DocumentElement.Name.Equals("RSAKeyValue"))
            {
                foreach (XmlNode childNode in xmlDocument.DocumentElement.ChildNodes)
                {
                    switch (childNode.Name)
                    {
                        case "Modulus":
                            parameters.Modulus = (string.IsNullOrEmpty(childNode.InnerText) ? null : Convert.FromBase64String(childNode.InnerText));
                            break;
                        case "Exponent":
                            parameters.Exponent = (string.IsNullOrEmpty(childNode.InnerText) ? null : Convert.FromBase64String(childNode.InnerText));
                            break;
                        case "P":
                            parameters.P = (string.IsNullOrEmpty(childNode.InnerText) ? null : Convert.FromBase64String(childNode.InnerText));
                            break;
                        case "Q":
                            parameters.Q = (string.IsNullOrEmpty(childNode.InnerText) ? null : Convert.FromBase64String(childNode.InnerText));
                            break;
                        case "DP":
                            parameters.DP = (string.IsNullOrEmpty(childNode.InnerText) ? null : Convert.FromBase64String(childNode.InnerText));
                            break;
                        case "DQ":
                            parameters.DQ = (string.IsNullOrEmpty(childNode.InnerText) ? null : Convert.FromBase64String(childNode.InnerText));
                            break;
                        case "InverseQ":
                            parameters.InverseQ = (string.IsNullOrEmpty(childNode.InnerText) ? null : Convert.FromBase64String(childNode.InnerText));
                            break;
                        case "D":
                            parameters.D = (string.IsNullOrEmpty(childNode.InnerText) ? null : Convert.FromBase64String(childNode.InnerText));
                            break;
                    }
                }
                rsa.ImportParameters(parameters);
                return;
            }
            throw new Exception("Invalid XML RSA key.");
        }

        public static string ToXmlString(this RSA rsa, bool includePrivateParameters)
        {
            RSAParameters rSAParameters = rsa.ExportParameters(includePrivateParameters);
            return $"<RSAKeyValue><Modulus>{((rSAParameters.Modulus != null) ? Convert.ToBase64String(rSAParameters.Modulus) : null)}</Modulus><Exponent>{((rSAParameters.Exponent != null) ? Convert.ToBase64String(rSAParameters.Exponent) : null)}</Exponent><P>{((rSAParameters.P != null) ? Convert.ToBase64String(rSAParameters.P) : null)}</P><Q>{((rSAParameters.Q != null) ? Convert.ToBase64String(rSAParameters.Q) : null)}</Q><DP>{((rSAParameters.DP != null) ? Convert.ToBase64String(rSAParameters.DP) : null)}</DP><DQ>{((rSAParameters.DQ != null) ? Convert.ToBase64String(rSAParameters.DQ) : null)}</DQ><InverseQ>{((rSAParameters.InverseQ != null) ? Convert.ToBase64String(rSAParameters.InverseQ) : null)}</InverseQ><D>{((rSAParameters.D != null) ? Convert.ToBase64String(rSAParameters.D) : null)}</D></RSAKeyValue>";
        }
    }
}
