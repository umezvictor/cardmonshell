﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Connect.Core;
using Connect.Core.Models;
using Connect.Core.Models.Map;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;


namespace Connect.Core.EF
{
    public class AppDbContext : IdentityDbContext, IDbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
            
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }
            
            modelBuilder.ApplyConfiguration(new MenuMap());
            base.OnModelCreating(modelBuilder);
        }

        public static string GetFileWithName(string filePath)
        {
            var baseDir = $@"{AppDomain.CurrentDomain.BaseDirectory}";

            if (Directory.Exists($"{baseDir}\bin"))
                return $@"{baseDir}\\bin{filePath}";
            else
                return $@"{baseDir}\{filePath}";
        }

        /// <summary>
        /// Attach an entity to the context or return an already attached entity (if it was already attached)
        /// </summary>
        /// <typeparam name="TEntity">TEntity</typeparam>
        /// <param name="entity">Entity</param>
        /// <returns>Attached entity</returns>
        protected virtual TEntity AttachEntityToContext<TEntity>(TEntity entity) where TEntity : AuditedEntity, new()
        {
            //little hack here until Entity Framework really supports stored procedures
            //otherwise, navigation properties of loaded entities are not loaded until an entity is attached to the context
            var alreadyAttached = Set<TEntity>().Local.FirstOrDefault(x => x.Id == entity.Id);
            if (alreadyAttached == null)
            {
                //attach new entity
                Set<TEntity>().Attach(entity);
                return entity;
            }
            else
            {
                //entity is already loaded.
                return alreadyAttached;
            }
        }

        /// <summary>
        /// Execute stores procedure and load a list of entities at the end
        /// </summary>
        /// <typeparam name="TEntity">Entity type</typeparam>
        /// <param name="commandText">Command text</param>
        /// <param name="parameters">Parameters</param>
        /// <returns>Entities</returns>
        public IList<TEntity> ExecuteStoredProcedureList<TEntity>(string commandText, params object[] parameters) where TEntity : AuditedEntity, new()
        {
            //add parameters to command
            if (parameters != null && parameters.Length > 0)
            {
                for (int i = 0; i <= parameters.Length - 1; i++)
                {
                    if (!(parameters[i] is DbParameter p))
                        throw new Exception("Not support parameter type");

                    commandText += i == 0 ? " " : ", ";

                    commandText += "@" + p.ParameterName;
                    if (p.Direction == ParameterDirection.InputOutput || p.Direction == ParameterDirection.Output)
                    {
                        //output parameter
                        commandText += " output";
                    }
                }
            }

            var result = SqlQuery<TEntity>(commandText, parameters).ToList();

            for (int i = 0; i < result.Count; i++)
                result[i] = AttachEntityToContext(result[i]);

            return result;
        }

        /// <summary>
        /// Creates a raw SQL query that will return elements of the given generic type.  The type can be any type that has properties that match the names of the columns returned from the query, or can be a simple primitive type. The type does not have to be an entity type. The results of this query are never tracked by the context even if the type of object returned is an entity type.
        /// </summary>
        /// <typeparam name="TElement">The type of object returned by the query.</typeparam>
        /// <param name="sql">The SQL query string.</param>
        /// <param name="parameters">The parameters to apply to the SQL query string.</param>
        /// <returns>Result</returns>
        public IQueryable<TElement> SqlQuery<TElement>(string sql, params object[] parameters) where TElement : AuditedEntity
        {
            return base.Set<TElement>().FromSqlRaw<TElement>(sql, parameters);
        }

        /// <summary>
        /// Executes the given DDL/DML command against the database.
        /// </summary>
        /// <param name="query">The command string</param>
        /// <param name="timeout">Timeout value, in seconds. A null value indicates that the default value of the underlying provider will be used</param>
        /// <param name="parameters">The parameters to apply to the command string.</param>
        /// <returns>The result returned by the database after executing the command.</returns>
        public int ExecuteSqlCommand(FormattableString query, int? timeout = null)
        {
            if (timeout.HasValue)
                this.Database.SetCommandTimeout(timeout);

            var result = this.Database.ExecuteSqlInterpolated(query);//.ExecuteSqlInterpolated(query);
            return result;
        }

        /// <summary>
        /// Executes the given DDL/DML command against the database.
        /// </summary>
        /// <param name="query">The command string</param>
        /// <param name="timeout">Timeout value, in seconds. A null value indicates that the default value of the underlying provider will be used</param>
        /// <param name="parameters">The parameters to apply to the command string.</param>
        /// <returns>The result returned by the database after executing the command.</returns>
        public async Task<int> ExecuteSqlCommandAsync(FormattableString query, int? timeout = null)
        {
            if (timeout.HasValue)
               this.Database.SetCommandTimeout(timeout);

            var result = await this.Database.ExecuteSqlInterpolatedAsync(query);
            return result;
        }

        public long BulkInsert(DataTable Data, string TableName, int BatchSize, int NotifySize)
        {
            SqlConnection sqlCon = (SqlConnection)Database.GetDbConnection();
            long insertedCount = 0;
            using (sqlCon)
            {
                try
                {
                    sqlCon.Open();
                    var transaction = sqlCon.BeginTransaction();
                    using (SqlBulkCopy sqlCopy = new SqlBulkCopy(sqlCon))
                    {

                        Data.Columns.Cast<DataColumn>().ToList().ForEach(f =>
                        {
                            SqlBulkCopyColumnMapping bccm = new SqlBulkCopyColumnMapping();
                            bccm.DestinationColumn = f.ColumnName;
                            bccm.SourceColumn = f.ColumnName;
                            sqlCopy.ColumnMappings.Add(bccm);
                        });
                        sqlCopy.NotifyAfter = NotifySize;
                        sqlCopy.SqlRowsCopied += (sender, eventArgs) => insertedCount = eventArgs.RowsCopied;
                        sqlCopy.DestinationTableName = TableName;
                        sqlCopy.BatchSize = BatchSize;
                        sqlCopy.WriteToServer(Data);
                    }
                    return insertedCount;
                }
                catch (Exception ex)
                {
                    throw ex;
                } 
            }
        }
       
    }
}
