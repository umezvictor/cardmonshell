﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Connect.Core.Models
{
    public class Menu : BaseEntity
    {
        public string Title { get; set; }
        public string Parameter { get; set; }
        public string InputType { get; set; }
    }

    
}
