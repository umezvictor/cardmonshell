﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using Connect.Core.Utils;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Connect.Core.Services
{
    public class MediaService : ControllerBase, IMediaService
    {
       
        public async Task<string> UploadImage(IFormFile file, string fileName)
        {
            //specify folder to save uploaded files
            var filePath = Path.Combine("Uploads/Images", fileName);

            // save profile picture
            using (var fileStream = new FileStream(filePath, FileMode.Create))
            {
                try
                {
                    await file.CopyToAsync(fileStream);
                    return "OK";
                }
                catch (Exception ex)
                {
                    return ex.ToString();
                }
            }

        }


        public async Task<string> UploadDocument(IFormFile file, string fileName)
        {
            //specify folder to save uploaded files
            var filePath = Path.Combine("Uploads/Docs", fileName);

            // save profile picture
            using (var fileStream = new FileStream(filePath, FileMode.Create))
            {
                try
                {
                    await file.CopyToAsync(fileStream);
                    return "OK";
                }
                catch (Exception ex)
                {
                    return ex.ToString();
                }
            }

        }


        public async Task<string> UpdateImage(IFormFile file, string oldFileName, string newFileName)
        {
            //oldfilename is the image path/name saved in the db when it was uplaoded

            if (System.IO.File.Exists(Path.Combine("Uploads/Images", oldFileName)))
            {
                //delete file if it exists
                System.IO.File.Delete(Path.Combine("Uploads/Images", oldFileName));
            }

            //new file
            var filePath = Path.Combine("Uploads/Images", newFileName);

            // save profile picture
            using (var fileStream = new FileStream(filePath, FileMode.Create))
            {
                try
                {
                    await file.CopyToAsync(fileStream);
                    return "OK";
                }
                catch (Exception ex)
                {
                    return ex.ToString();
                }
            }

        }


        public async Task<string> UpdateDocument(IFormFile file, string oldFileName, string newFileName)
        {
            //oldfilename is the image path/name saved in the db when it was uplaoded

            if (System.IO.File.Exists(Path.Combine("Uploads/Docs", oldFileName)))
            {
                //delete file if it exists
                System.IO.File.Delete(Path.Combine("Uploads/Docs", oldFileName));
            }

            //new file
            var filePath = Path.Combine("Uploads/Docs", newFileName);

            // save profile picture
            using (var fileStream = new FileStream(filePath, FileMode.Create))
            {
                try
                {
                    await file.CopyToAsync(fileStream);
                    return "OK";
                }
                catch (Exception ex)
                {
                    return ex.ToString();
                }
            }

        }

        public IActionResult DisplayImage(string fileName)
        {
            try
            {
                string fileExtension = "";
                //image path
                string imagePath = Path.Combine("Uploads/Images", fileName);
                //get file extension
                string extension = Path.GetExtension(imagePath).TrimStart('.');

                //note, only 'jpeg' and 'png' extensions can be rendered, 'jpg' will return gibberish characters

                if (extension == "jpeg")
                {
                    fileExtension = "jpeg";
                }
                if (extension == "jpg")
                {
                    fileExtension = "jpeg";
                }
                if (extension == "png")
                {
                    fileExtension = "png";
                }
                
                //ensure to inherit from ControllerBase -- scroll up
               
                Byte[] b = System.IO.File.ReadAllBytes(imagePath);                     
                return File(b, "image/" + fileExtension);

            }
            catch (Exception ex)
            {
                Logger.writeErrorLog(ex.Message);
                return null;
            }
        }       

    }
}
