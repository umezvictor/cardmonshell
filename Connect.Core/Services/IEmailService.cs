﻿using System.Threading.Tasks;

namespace Connect.Core.Services
{
    public interface IEmailService
    {
        Task SendEmail(string toEmail, string subject, string body);
    }
}