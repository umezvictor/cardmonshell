﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Connect.Core.Services
{
    public interface IMediaService
    {
        IActionResult DisplayImage(string fileName);
        Task<string> UpdateDocument(IFormFile file, string oldFileName, string newFileName);
        Task<string> UpdateImage(IFormFile file, string oldFileName, string newFileName);
        Task<string> UploadDocument(IFormFile file, string fileName);
        Task<string> UploadImage(IFormFile file, string fileName);
    }
}