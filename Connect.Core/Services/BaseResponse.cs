﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Connect.Core.Services
{
    public class MainResponse<T> : BaseResponse
    {
        public T Result { get; set; }
    }

    public class BaseResponse
    {
        public Response Response { get; set; }
        public Exception Exception { get; set; }

        public string Message { get; set; }
    }

    public enum Response
    {
        OK=0,
        NOTOK=1,
        ERROR=2,
        EMPTY=3
    }


    

}
