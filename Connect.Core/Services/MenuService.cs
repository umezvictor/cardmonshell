﻿
using Connect.Core.Interfaces;
using Connect.Core.Services;
using Connect.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Connect.Core.Models
{
    public class MenuService : IMenuService
    {
        private readonly IService<Menu> discRepo;


        public MenuService(IService<Menu> discRepo, IUnitOfWork uow)
        {
            this.discRepo = discRepo;

        }

        public async Task<MainResponse<object>> AddMenu(Menu menu)
        {
            MainResponse<object> mainResponse = new MainResponse<object>();

            try
            {
                await discRepo.AddAsync(menu);
                mainResponse.Response = Response.OK;
            }
            catch (Exception ex)
            {
                mainResponse.Response = Response.ERROR;
                mainResponse.Exception = ex;
            }

            return mainResponse;
        }

        public async Task<MainResponse<IEnumerable<Menu>>> GetMenus(string menu)
        {

            MainResponse<IEnumerable<Menu>> mainResponse = new MainResponse<IEnumerable<Menu>>();

            try
            {
                //var parameters = new Dictionary<string, string>()
                //{
                //    { "Title", vm.Title}
                //};

                mainResponse.Result = await discRepo.SqlQuery<Menu>($"select * from Menu where Title = '{menu}'", null);
                mainResponse.Response = Response.OK;
            }
            catch (Exception ex)
            {
                mainResponse.Response = Response.ERROR;
                mainResponse.Exception = ex;
            }

            return mainResponse;
        }

        public async Task<MainResponse<Menu>> GetMenuById(Guid id)
        {
            MainResponse<Menu> mainResponse = new MainResponse<Menu>();

            try
            {
                mainResponse.Result = await discRepo.FindById(id);
                mainResponse.Response = Response.OK;
            }
            catch (Exception ex)
            {
                mainResponse.Response = Response.ERROR;
                mainResponse.Exception = ex;
            }

            return mainResponse;
        }

        public async Task<MainResponse<object>> DeleteMenu(Guid Id)
        {
            MainResponse<object> mainResponse = new MainResponse<object>();

            try
            {

                await discRepo.SqlQuery<Menu>($"update Menu set IsDeleted = 'true' where Id = '{Id}'", null);
                mainResponse.Response = Response.OK;
            }
            catch (Exception ex)
            {
                mainResponse.Response = Response.ERROR;
                mainResponse.Exception = ex;
            }

            return mainResponse;
        }

    }
}
