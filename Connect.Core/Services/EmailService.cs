﻿using Connect.Core.Utils;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Connect.Core.Services
{
    //for gmail, click here to enable less secure apps - for development purpose
    //https://myaccount.google.com/lesssecureapps?pli=1  
    public class EmailService : IEmailService
    {
        public async Task SendEmail(string toEmail, string subject, string body)
        {
            //get email settings
            var emailConfig = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("EmailSettings");

            string address = emailConfig["MailAddress"];
            string senderName = emailConfig["SenderName"];
            string password = emailConfig["MailPassword"];
            string host = emailConfig["Host"];
            string port = emailConfig["Port"];


            MailMessage message = new MailMessage();

            message.To.Add(toEmail);
            message.Subject = (subject);
            message.From = new MailAddress(address, senderName, Encoding.UTF8);
            message.Body = body;
            message.IsBodyHtml = true;

            SmtpClient smtp = new SmtpClient();
            smtp.Host = host;
            smtp.EnableSsl = true;
            
            NetworkCredential NetworkCred = new NetworkCredential(address, password);
            smtp.UseDefaultCredentials = true;
            smtp.Credentials = NetworkCred;
            smtp.Port = Convert.ToInt32(port);
           //send email asynchronously
                await Task.Run(() => {
                    try
                    {
                        smtp.Send(message);
                    }
                    catch(Exception ex)
                    {
                        Logger.writeErrorLog(ex.Message); 
                    }                    
                }); 
        }
    }
}
