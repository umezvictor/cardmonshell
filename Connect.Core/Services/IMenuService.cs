﻿using Connect.Core.Services;
using Connect.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Connect.Core.Models
{
    public interface IMenuService
    {
        Task<MainResponse<object>> AddMenu(Menu menu);
        Task<MainResponse<object>> DeleteMenu(Guid Id);
        Task<MainResponse<Menu>> GetMenuById(Guid id);
        Task<MainResponse<IEnumerable<Menu>>> GetMenus(string menu);
    }
}