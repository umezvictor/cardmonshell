    const myForm = document.getElementById('myForm');
    const addBtn = document.getElementById('addBtn');
const myformRowDiv = document.getElementById('myformRowDiv'); // form row div

// default input fields that show on web page
const title = document.getElementById("title"); 
const parameter = document.getElementById("parameter");
const inputType = document.getElementById("inputType");

    addBtn.addEventListener('click', () => {

  // create a new form div
    const newDiv = document.createElement("div");
      newDiv.className = "form-group col-md-6";
    // create new label and input 
    const newLabel = document.createElement("label");
      const newInput = document.createElement("input");

      // create a name attribute for input element
      var nameAttribute = document.createAttribute("name");
      // set a value for attribute
      nameAttribute.value = "Parameter"; //
      // add attribute to input element
      newInput.setAttributeNode(nameAttribute);
    newInput.className = "form-control input-parameter";
    newLabel.textContent = "field";

      // append elements to new form div
      newDiv.appendChild(newLabel);
      newDiv.appendChild(newInput);
      // append form div to form row div
      myformRowDiv.append(newDiv);



    // select dropdown div

    //create second div for dropdown
    const dropdownDiv = document.createElement("div");
    dropdownDiv.className = "form-group col-md-4";
    // label for second div
    const dropdownLabel = document.createElement("label");
    dropdownLabel.textContent = "input type";
   
    // create dropdown - select element
     const dropdown = document.createElement("select");
      // create a name attribute for input element
     var dropdownNameAttribute = document.createAttribute("name");
      // set a value for attribute
     dropdownNameAttribute.value = "InputType"; //
      // add attribute to input element
    dropdown.setAttributeNode(dropdownNameAttribute);
    // add class to select dropdown
    dropdown.className = "form-control input-type";
    // create options
    const dropdownOption1 = document.createElement("option");
    const dropdownOption2 = document.createElement("option");
    const dropdownOption3 = document.createElement("option");
    const dropdownOption4 = document.createElement("option");
    // add text to options
    dropdownOption1.textContent = "text";
    dropdownOption2.textContent = "numeric";
    dropdownOption3.textContent = "dropdown";
    dropdownOption4.textContent = "file";
    //add option to dropdown
    dropdown.add(dropdownOption1);
    dropdown.add(dropdownOption2);
    dropdown.add(dropdownOption3);
    dropdown.add(dropdownOption4);

    // append elements to second div
    dropdownDiv.appendChild(dropdownLabel);
    dropdownDiv.appendChild(dropdown);
    // append second div to form row div
    myformRowDiv.append(dropdownDiv);

});



// submit form
    const saveBtn = document.getElementById('saveBtn');
    saveBtn.addEventListener('click', (event) => {
    event.preventDefault();

      var menuTitle = title.value;
      var menuParameter = parameter.value;
      var menuInputType = inputType.value;

      //  array to hold extra fields holding extra parameters
      var extraParamsArray = [];
      var extraInputTypeArray = [];

      var parameterArray = document.getElementsByClassName("input-parameter");
      var inputTypeArray = document.getElementsByClassName("input-type");

      for (i = 0; i < parameterArray.length; i++) {
        extraParamsArray.push(parameterArray[i].value);
       
      }

      for (j = 0; j < inputTypeArray.length; j++) {
        extraInputTypeArray.push(inputTypeArray[j].value);
      }

      //console.log(menuTitle);
      //console.log(menuParameter);
      //console.log(menuInputType);

      //console.log(extraParamsArray);
      //console.log(extraInputTypeArray);

      // make api request
      var data = {  
        Title: menuTitle,
        DefaultParameter: menuParameter,
        DefaultInputType: menuInputType,
        ExtraParams: extraParamsArray,
        ExtraInputTypes: extraInputTypeArray
      }


     // console.log(JSON.stringify(data));

      fetch('https://localhost:44317/Admin/AddMenu', {
        method: "POST",
        body: JSON.stringify(data),
        headers: { "Content-type": "application/json; charset=UTF-8" }
      })
        .then(response => response.json())
        .then(res => {
          console.log(res);
        })
        .catch(err => console.log(err));

});
