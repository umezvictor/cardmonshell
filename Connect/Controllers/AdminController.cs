﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Connect.Core.Services;
using Connect.Core.Models;
using Connect.Core.ViewModels;

namespace Connect.Controllers
{
    public class AdminController : Controller
    {
        private readonly IMenuService menuService;

        public AdminController(IMenuService menuService)
        {
            this.menuService = menuService;
        }
        

        // 08137807610
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        //public string Title { get; set; }
        //public string DefaultParameter { get; set; }
        //public string DefaultInputType { get; set; }
        //public List<string> ExtraParams { get; set; }
        //public List<string> ExtraInputTypes { get; set; }

        [HttpPost]
        public async Task<bool> AddMenu([FromBody] MenuVM model)
        {
            Menu menu = new Menu
            {
                Title = model.Title,
                Parameter = model.DefaultParameter,
                InputType = model.DefaultInputType
            };
            await menuService.AddMenu(menu);

            // amount - text -- title
            // account - numeric  -- title
            
            //save extra parametrs
            //foreach(var param in model.ExtraParams)
            //{
            //    foreach(var type in model.ExtraInputTypes)
            //    {
            //        Menu extras = new Menu
            //        {
            //            Title = model.Title,
            //            Parameter = param,
            //            InputType = type
            //        };
            //        await menuService.AddMenu(extras);
            //    }
            //}
           
            // to be continued
            for(int i = 0; i < model.ExtraParams.Count(); i++)
            {
                for(int j = 0; j == i; j++)
                {
                    Menu extras = new Menu
                    {
                        Title = model.Title,
                        Parameter = model.ExtraParams[i],
                        InputType = model.ExtraInputTypes[j]
                    };
                    await menuService.AddMenu(extras);
                }
                
            }            
            return true;
            
        }

        [HttpGet]
        public async Task<IActionResult> LoadPage(string menu)
        {
            var res = await menuService.GetMenus(menu);
            
            return View(res.Result);

        }

    }
}


//click on menu
//menu name will be rendered in a form and sent as a post
//go to db and find menu that matches menu name
//select the list of records from db
//send to ui
//    loop through records in ui
//    if input type = Text, render a text box
//    set parameneter as label
//    set parameter as name attribute 