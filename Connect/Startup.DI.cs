﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Connect.Core;
using Connect.Core.EF;
using Connect.Core.Interfaces;
using Connect.Core.Models;
using Connect.Core.Repository;
using Connect.Core.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Connect
{
    public partial class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureDIService(IServiceCollection services)
        {
            //INJECT ALL ENTITIES HERE, EG GROUPS, DISCIPLES         
            services.AddTransient<ITokenService, TokenService>();
       
            services.AddTransient<IEmailService, EmailService>();           
            services.AddTransient<IMediaService, MediaService>();       
            services.AddSingleton(typeof(IUnitOfWork), typeof(UnitOfWork));
            services.AddSingleton(typeof(IDapperRepository<>), typeof(DapperRepository<>));

            services.AddTransient<IDbConnection>(db =>
            {
                var connectionString = Configuration.GetConnectionString("Default");
                var connection = new SqlConnection(connectionString);
                return connection;
            });

            services.AddSingleton(typeof(IService<>), typeof(Service<>));
            services.AddScoped<IDbContext, AppDbContext>();
           
        }


    }
}
