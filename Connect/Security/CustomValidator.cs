﻿using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Logging;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace MSConsole.Security
{
    public class CustomValidator : ISecurityTokenValidator
    {
        private int _maxTokenSizeInBytes = TokenValidationParameters.DefaultMaximumTokenSizeInBytes;
        private readonly IConfiguration _config;
        public CustomValidator(IConfiguration config)
        {
            _config = config;
        }
        public bool CanValidateToken => true;

        public int MaximumTokenSizeInBytes { get => _maxTokenSizeInBytes; set => _maxTokenSizeInBytes = value; }

        public bool CanReadToken(string securityToken)
        {
            return string.IsNullOrEmpty(securityToken) ? false : true;
        }

        public ClaimsPrincipal ValidateToken(string securityToken, TokenValidationParameters validationParameters, out SecurityToken validatedToken)
        {
            IdentityModelEventSource.ShowPII = true;
            var handler = new JwtSecurityTokenHandler();
            var principal =  handler.ValidateToken(securityToken, validationParameters, out validatedToken);
            return principal;
        }
    }
}
