﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Connect.Core.EF;
using Connect.Core.Extensions;
using Connect.Core.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using MSConsole.Security;
using Microsoft.Extensions.Hosting;
using Connect.Core.Cache;
using System.Text;

namespace Connect
{
    public partial class Startup
    {


        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceCollection AddCustomIdentity(IServiceCollection services)
        {
            services.AddAuthentication()
               .AddJwtBearer(cfg =>
               {
                   cfg.SecurityTokenValidators.Clear();
                   cfg.SecurityTokenValidators.Add(new CustomValidator(Configuration));
                   cfg.RequireHttpsMetadata = false;
                   cfg.SaveToken = true;

                   var rsaKey = Configuration["Tokens:SecretKey"];
                   RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
                   rsa.FromXmlString2(rsaKey);
                   var rSAParameters = rsa.ExportParameters(false);
                   var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(rsaKey));

                   cfg.TokenValidationParameters = new TokenValidationParameters()
                   {
                       ValidateIssuer = false,
                       ValidateAudience = false,
                       ValidateLifetime = HostEnvironment.IsDevelopment() ? false : false,
                       ValidateIssuerSigningKey = true,
                       ValidIssuer = Configuration["Tokens:Issuer"],
                       ValidAudience = Configuration["Tokens:Issuer"],
                       IssuerSigningKey = key,//new RsaSecurityKey(rSAParameters),
                       ClockSkew = TimeSpan.Zero
                   };

                   cfg.Events = new JwtBearerEvents()
                   {
                       OnTokenValidated = async context =>
                       {
                           var claims = context.Principal;
                           var username = claims.FindFirst("user_name");

                           if (username == null)
                               return;

                           var _sharedCache = context.HttpContext.RequestServices.GetRequiredService<ISharedCache>();
                           var cachedClaims = _sharedCache.Get<IEnumerable<Claim>>(username.Value);

                           if (cachedClaims == null || !cachedClaims.Any())
                               return;

                           var userNameClaim = cachedClaims.FirstOrDefault(c => c.Type == JwtRegisteredClaimNames.Sub);
                           var userIdClaim = cachedClaims.FirstOrDefault(c => c.Type == JwtRegisteredClaimNames.Jti);

                           if (userIdClaim == null || userNameClaim == null)
                               return;

                           var claimIdentity = new ClaimsIdentity();
                           claimIdentity.AddClaim(userNameClaim);
                           claims.AddIdentity(claimIdentity);
                           await Task.FromResult(0);
                       }
                   };
               });

            //services.AddIdentity <ConnectUser, ConnectRole>(options =>
            //{
            //    options.Password.RequireNonAlphanumeric = false;
            //    options.User.RequireUniqueEmail = true;
            //    options.Lockout.AllowedForNewUsers = false;
            //    options.Password.RequireDigit = false;
            //    options.Password.RequireLowercase = false;
            //    options.Password.RequireUppercase = false;
            //    options.Password.RequiredLength = 6;
            //})
            //.AddEntityFrameworkStores<AppDbContext>()
            //.AddDefaultTokenProviders();

            services.Configure<DataProtectionTokenProviderOptions>(options =>
            {
                options.TokenLifespan = TimeSpan.FromHours(24);
            });

            return services;
        }


    }
}
